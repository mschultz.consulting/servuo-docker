FROM ubuntu:20.04

RUN apt-get update && apt-get install -y wget mono-complete

#RUN git clone https://github.com/ServUO/ServUO.git

RUN mkdir /ServUO

WORKDIR /ServUO 

EXPOSE 2593

ENTRYPOINT ["mono","ServUO.exe"]